Single:

https://www.freesoundeffects.com/free-track/headchop-466431/
https://www.freesoundeffects.com/free-track/hit-arg-466433/


https://www.freesoundeffects.com/free-track/wolf2-466394/
https://www.freesoundeffects.com/free-track/lidcreak-466370/
https://www.freesoundeffects.com/free-track/heartbeat-466362/


https://freesound.org/people/Robinhood76/sounds/66645/ nazgul
https://freesound.org/people/Robinhood76/sounds/94281/ monster dying
https://freesound.org/people/Robinhood76/sounds/161535/ long monster sounds
https://freesound.org/people/Robinhood76/sounds/124387/ monster moans

Packs:

https://www.rocketstock.com/free-after-effects-templates/horror-sound-effects-pack/
https://www.premiumbeat.com/blog/40-free-footstep-foley-sound-effects/
http://soundbible.com/tags-horror.html

Requirements:

Flames crackle + match strike when turned on
footsteps of player with echo and reverb
monster appears with screeching
blood spatters killing sound brutal
mechanical trap sound
jail door opening rusted when going from level to level
bat sounds
monster sound when lights turn off
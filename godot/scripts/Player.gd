extends Node2D

var _can_move :bool = false
var light_dir : Vector2 = Vector2()

var hurt = false

# Called when the node enters the scene tree for the first time.
func _ready():
	$LightAnimator.play("Waver")
	$AnimationPlayer.play("Idle")
	$RayCast2D.add_exception($Area2D)
	$RayCast2D.add_exception($HitArea)
	light_dir = $Light2D.position.normalized()
	
	var target_pos_candle = Vector2()
	var target_pos_face = Vector2()
	if light_dir.x == 1:
		target_pos_candle = $candle_pos/right.position
		target_pos_face = $Body/Body/face_right.position
		$Body/candle.z_index = 1
		$Body/Body/Face.visible = true
	elif light_dir.x == -1:
		target_pos_candle = $candle_pos/left.position
		target_pos_face = $Body/Body/face_left.position
		$Body/candle.z_index = 1
		$Body/Body/Face.visible = true
	elif light_dir.y == 1:
		target_pos_candle = $candle_pos/down.position
		target_pos_face = $Body/Body/face_mid.position
		$Body/candle.z_index = 1
		$Body/Body/Face.visible = true
	elif light_dir.y == -1:
		target_pos_candle = $candle_pos/up.position
		target_pos_face = $Body/Body/face_up.position		
		$Body/candle.z_index = 0
		$Body/Body/Face.visible = true
		
	$Body/Body/Face.position = target_pos_face
	$Body/candle.position = target_pos_candle
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	
	handle_movement(delta)
	
func handle_movement(delta):
	if hurt:
		return
	if(_can_move and not $moveTween.is_active()):
		if(Input.is_action_pressed("toggle_candle")):
			use_turn(0.25)
#			$Light2D.visible = not $Light2D.visible
			toggle_player_light(not $PlayerLight.visible)
			var mobs = get_tree().get_nodes_in_group("monsters")
			for mob in mobs:
				mob.light_toggled()
			return
			
		var _target_dir = Vector2()
		if(Input.is_action_pressed("move_down")):
			_target_dir.y += 1
		elif(Input.is_action_pressed("move_up")):
			_target_dir.y -= 1
		elif(Input.is_action_pressed("move_right")):
			_target_dir.x += 1
		elif(Input.is_action_pressed("move_left")):
			_target_dir.x -= 1
		
		if _target_dir:
			$RayCast2D.cast_to = _target_dir * global.tile_size
			$RayCast2D.force_update_transform()
			$RayCast2D.force_raycast_update()
			var thing = $RayCast2D.get_collider()
			if _target_dir == light_dir:
				if thing:
	#				if thing.get_parent().has_method("can_react_to_light") and thing.get_parent().can_react_to_light():
	#					use_turn()
	#					thing.get_parent().react_to_light()
	#				else:
					if thing.get_parent().has_method("allow_player_move") and thing.get_parent().allow_player_move():
						move(_target_dir)
					if $PlayerLight.visible:
#						print("Monster blocking")
						if affect_with_light(_target_dir):
							print("Monster activated")
							return
#							$AnimationPlayer.play("Hurt")
#							print("You accidentally run into something")
					if thing.get_parent().has_method("harm_player") and thing.get_parent().harm_player():
						move(_target_dir)
#							yield(get_tree().create_timer(0.5), "timeout")
#							$RayCast2D.cast_to = light_dir * global.tile_size / 4
#							$RayCast2D.force_update_transform()
#							$RayCast2D.force_raycast_update()
#							var new_thing = $RayCast2D.get_collider()
#							if new_thing and new_thing.get_parent().has_method("harm_player") and new_thing.get_parent().harm_player():
#								get_hurt(new_thing.get_parent())
#					else:
#						move(_target_dir)
#						$AnimationPlayer.play("Hurt")
#						print("You accidentally run into something")
				else:
					move(_target_dir)
			else:
				aim_light(_target_dir)
				if $PlayerLight.visible:
					use_turn(0.25)

func move(dir):
	use_turn()
	$AnimationPlayer.play("Jump")
	$moveTween.interpolate_property(self,"position", position, position + (global.tile_size * dir
	), 0.5, Tween.TRANS_SINE, Tween.EASE_OUT)
	$moveTween.start()
	play_move_sound()

var last_move_sound = null
func play_move_sound():
	if last_move_sound == "sfx/walk_1":
		last_move_sound = "sfx/walk_2"
	else:
		last_move_sound = "sfx/walk_1"
	get_node(last_move_sound).play()

func use_turn(wait = 0.5):
	$turnTimer.wait_time = wait
	_can_move = false
	$turnTimer.start()
	print("Used turn.")

func _on_turnTimer_timeout():
	_can_move = true

func aim_light(dir):
	light_dir = dir
	$LightAnimator.stop()
	$Light2D/Tween.interpolate_property(
	$Light2D, "position", $Light2D.position, dir * global.tile_size, 0.25, Tween.TRANS_SINE,Tween.EASE_OUT)
	var target_pos_candle = Vector2()
	var target_pos_face = Vector2()
	if dir.x == 1:
		target_pos_candle = $candle_pos/right.position
		target_pos_face = $Body/Body/face_right.position
		$Body/candle.z_index = 1
		$Body/Body/Face.visible = true
	elif dir.x == -1:
		target_pos_candle = $candle_pos/left.position
		target_pos_face = $Body/Body/face_left.position
		$Body/candle.z_index = 1
		$Body/Body/Face.visible = true
	elif dir.y == 1:
		target_pos_candle = $candle_pos/down.position
		target_pos_face = $Body/Body/face_mid.position
		$Body/candle.z_index = 1
		$Body/Body/Face.visible = true
	elif dir.y == -1:
		target_pos_candle = $candle_pos/up.position
		target_pos_face = $Body/Body/face_up.position		
		$Body/candle.z_index = 0
		$Body/Body/Face.visible = true
	$Light2D/Tween.interpolate_property(
	$Body/candle, "position", $Body/candle.position, target_pos_candle, 0.25, Tween.TRANS_SINE,Tween.EASE_OUT)
	$Light2D/Tween.interpolate_property(
		$Body/Body/Face, "position", $Body/Body/Face.position, target_pos_face, 0.25, Tween.TRANS_SINE,Tween.EASE_OUT)
	$Light2D/Tween.start()
	if $PlayerLight.visible:
		$Light2D/AnimationPlayer.play("Shrink")
		yield($Light2D/AnimationPlayer,"animation_finished")
		affect_with_light(light_dir)
		$LightAnimator.play("Waver")

func _on_moveTween_tween_completed(object, key):
	if $PlayerLight.visible:
		affect_with_light(light_dir)

func affect_with_light(dir):
	$RayCast2D.cast_to =  dir * global.tile_size
	$RayCast2D.force_update_transform()
	$RayCast2D.force_raycast_update()
	var thing = $RayCast2D.get_collider()
	if thing and  thing.get_parent().has_method("can_react_to_light"):
		if thing.get_parent().can_react_to_light():
			$sfx/gasp.play()
			use_turn()
			thing.get_parent().react_to_light()
			return true
	return false
#		if thing.get_parent().has_method("harm_player") and thing.get_parent().harm_player():
#			return true


func _on_Light_tween_completed(object, key):
	if $PlayerLight.visible:
		affect_with_light(light_dir)
	else:
		print("Nothing")
		
func _on_lightToggle_tween_completed(object, key):
	if $PlayerLight.visible:
		affect_with_light(light_dir)


func _on_HitArea_area_entered(area):
	if area.get_parent().has_method("harm_player") and area.get_parent().harm_player():
		get_hurt(area.get_parent())


func get_hurt(new_thing):
	if not new_thing.has_method("harm_monster"):
		$moveTween.stop_all()
	if $AnimationPlayer.is_playing() and $AnimationPlayer.current_animation == "Jump":
		yield($AnimationPlayer, "animation_finished")
#	$AnimationPlayer.play("Hurt")
	hurt = true
	global.bgm.fade_out(global.current_mood)
	toggle_player_light(false)
	if new_thing.has_method("react_to_hurting_player"):
		new_thing.react_to_hurting_player(self)
	print("You accidentally run into something")
	
	$sfx/scream.play()
	
	var fades = get_tree().get_nodes_in_group("fade_anim")
	if fades.size()>0:
		print("Fading out")
		var fade_anim = fades[0]
		fade_anim.play("fade_in")
		yield(fade_anim, "animation_finished")
	get_tree().reload_current_scene()
	

func toggle_player_light(_enabled):
	if $PlayerLight.visible == _enabled:
		return
	
	$LightAnimator.stop()
	if not _enabled:
		$sfx/candle_off.play()
		$PlayerLight/Tween.interpolate_property(
		$Light2D, "scale", Vector2(1,1),Vector2(0,0), 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
		$PlayerLight/Tween.interpolate_property(
		$Light2D, "energy", 1,0, 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
		$PlayerLight/Tween.interpolate_property(
		$Body/candle, "modulate", $Body/candle.modulate,Color("00ffffff"), 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
		$PlayerLight/Tween.interpolate_property(
		$Body/Shadow, "modulate", $Body/Shadow.modulate,Color("00ffffff"), 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
		$PlayerLight/Tween.interpolate_property(
		$sfx/candle_mood, "volume_db", $sfx/candle_mood.volume_db,-60, 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
		
		if not hurt:
			$PlayerLight/Tween.interpolate_property($Body/Body, "self_modulate", $Body/candle.self_modulate,Color(
			"00ffffff"), 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
		else:
			$PlayerLight/Tween.interpolate_property($Body/Body, "modulate", $Body/candle.modulate,Color(
			"00ffffff"), 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
		$PlayerLight/Tween.start()
		$PlayerLight.visible = _enabled
		yield($PlayerLight/Tween, "tween_completed")
		$LightAnimator.play("Waver")
	else:
		$sfx/candle_on.play()
		$PlayerLight/Tween.interpolate_property(
		$Light2D, "scale", Vector2(0,0),Vector2(1,1), 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
		$PlayerLight/Tween.interpolate_property(
		$Light2D, "energy", 0,1, 0.25, Tween.TRANS_SINE, Tween.EASE_OUT)
		$sfx/candle_mood.volume_db = 0
		
		$Body/candle.modulate = Color("ffffff")
		$Body/Body.self_modulate = Color("ffffff")
		$Body/Shadow.modulate = Color("ffffff")
		$PlayerLight/Tween.start()
		$PlayerLight.visible = _enabled
		$LightAnimator.play("Waver")


func wait_to_finish_moving():
	if $moveTween.is_active():
		yield($moveTween,"tween_completed")


func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Jump":
		$AnimationPlayer.play("Idle")


func _on_monster_check_timeout():
	var mons = get_tree().get_nodes_in_group("monsters")
	var close_intensity = 0
	for mon in mons:
		if mon._state == mon.STATE.ANGRY: 
			if (mon.position - position).length() / global.tile_size < 2:
				close_intensity = max(close_intensity, 3)
			elif (mon.position - position).length() / global.tile_size < 4:
				close_intensity = max(close_intensity, 2)
			elif (mon.position - position).length() / global.tile_size < 6:
				close_intensity = max(close_intensity, 1)
	
	global.bgm.fade_chase_intensity(close_intensity)

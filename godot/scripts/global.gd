extends Node

export(int) var tile_size = 256

var bgm = preload("res://Scenes/bgm.tscn").instance()

var last_bgm_pos

var setting_levels_finished = 0

var current_mood = "horror_mood"

# Called when the node enters the scene tree for the first time.
func _ready():
	add_child(bgm)
	pass # Replace with function body.
	
func _process(delta):
	if Input.is_action_just_released("exit"):
		Persist.save_game()
		get_tree().quit()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func level_finished():
	setting_levels_finished += 1
	Persist.save_game()
	var transitions = get_tree().get_nodes_in_group("transition_anim")
	var anim_node :AnimationPlayer = transitions[0]
	bgm.fade_out(current_mood)
	anim_node.play("go_in")
	yield(anim_node,"animation_finished")
	get_tree().change_scene("res://Scenes/Tower.tscn")

func force_change_scene():
	get_tree().paused = false
	get_tree().change_scene("res://Scenes/Tower.tscn")

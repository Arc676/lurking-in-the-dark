extends Node2D

enum STATE {
	ACTIVE,
	DISABLED
}

var _state = STATE.ACTIVE

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func harm_player():
	return _state == STATE.ACTIVE

func react_to_hurting_player(player):
	$sfx/spikes.play()
	$Sprite.playing = true
	_state = STATE.DISABLED
	$Sprite/spikes/AnimationPlayer.play("eject")
	$BloodSplatter.emitting = true

func harm_monster():
	return _state == STATE.ACTIVE

func react_to_hurting_monster():
	$sfx/spikes.play()	
	$Sprite.use_parent_material = false
	$Sprite.playing = true
	_state = STATE.DISABLED
	$Sprite/spikes/AnimationPlayer.play("eject")
	$BloodSplatter.emitting = true


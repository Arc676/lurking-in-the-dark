extends Node2D



# Called when the node enters the scene tree for the first time.
func _ready():
	global.bgm.fade_in(global.current_mood)
	get_node("CanvasLayer/Transition").visible = true
	material.light_mode = CanvasItemMaterial.LIGHT_MODE_LIGHT_ONLY
	$Floor/BG.visible = false
	
	if not get_node("Title Screen"):
		var match_sounds = get_tree().get_nodes_in_group("match_sound")
		if match_sounds.size()>0:
			match_sounds[0].play()
		get_node("CanvasLayer/Transition/AnimationPlayer").play("go_out")
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
